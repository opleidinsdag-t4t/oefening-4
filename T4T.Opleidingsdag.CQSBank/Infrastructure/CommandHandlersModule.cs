﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace T4T.Opleidingsdag.CQSBank.Infrastructure
{
    public abstract class CommandHandlersModule<TConnection>
    {
        private readonly Dictionary<Type, Func<TConnection, object, Task<CommandExecutionResult>>> _handlers;

        protected CommandHandlersModule()
        {
            _handlers = new Dictionary<Type, Func<TConnection, object, Task<CommandExecutionResult>>>();
        }

        /// <summary>
        /// Registers a new asynchronous command handler for the given command.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        /// <param name="handler">The handler.</param>
        protected void Register<TCommand>(Func<TConnection, TCommand, Task<CommandExecutionResult>> handler)
        {
            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            if (_handlers.ContainsKey(typeof(TCommand)))
                throw new InvalidOperationException($"There is already a handler registered for the commmand {typeof(TCommand).FullName}!");

            _handlers.Add(typeof(TCommand), (connection, command) => handler(connection, (TCommand)command));
        }

        /// <summary>
        /// Finds the registered handler for the given commmand and invokes it. Throws when no handler is registered.
        /// </summary>
        /// <typeparam name="TCommand">The type of the command to handle.</typeparam>
        /// <param name="command">The command.</param>
        /// <param name="connection">The connection.</param>
        public async Task<CommandExecutionResult> HandleAsync<TCommand>(TCommand command, TConnection connection)
        {
            if (!_handlers.TryGetValue(command.GetType(), out var handler))
                throw new InvalidOperationException($"No handler is registered for command {typeof(TCommand).FullName}!");

            return await handler.Invoke(connection, command);
        }

    }
}

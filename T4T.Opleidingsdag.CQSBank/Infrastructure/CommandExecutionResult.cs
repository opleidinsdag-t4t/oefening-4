﻿using System;

namespace T4T.Opleidingsdag.CQSBank.Infrastructure
{
    public class CommandExecutionResult
    {
        public bool Success { get; set; } = true;
        public string ErrorMessage { get; private set; }
        public Exception Exception { get; private set; }

        public void SetException(Exception exception)
        {
            Success = false;
            ErrorMessage = exception.Message;
            Exception = Exception;
        }
    }
}

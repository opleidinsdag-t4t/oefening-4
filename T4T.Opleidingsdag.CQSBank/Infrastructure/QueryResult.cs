﻿namespace T4T.Opleidingsdag.CQSBank.Infrastructure
{
    public abstract class QueryResult
    {
        public bool Success { get; set; } = true;
    }
}

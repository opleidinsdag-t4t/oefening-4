﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace T4T.Opleidingsdag.CQSBank.Infrastructure
{
    public abstract class QueryHandlersModule<TConnection>
    {
        private readonly Dictionary<Type, Func<TConnection, object, Task<QueryResult>>> _handlers;

        protected QueryHandlersModule()
        {
            _handlers = new Dictionary<Type, Func<TConnection, object, Task<QueryResult>>>();
        }

        /// <summary>
        /// Registers a new asynchronous query handler for the given query.
        /// </summary>
        /// <typeparam name="TQuery">The type of the query.</typeparam>
        /// <typeparam name="TQueryResult">The type of the queryResult.</typeparam>
        /// <param name="handler">The handler.</param>
        protected void Register<TQuery, TQueryResult>(Func<TConnection, TQuery, Task<TQueryResult>> handler) where TQueryResult : QueryResult
        {
            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            if (_handlers.ContainsKey(typeof(TQuery)))
                throw new InvalidOperationException($"There is already a handler registered for the query {typeof(TQuery).FullName}!");

            _handlers.Add(typeof(TQuery), async (connection, query) => await handler(connection, (TQuery)query));
        }

        /// <summary>
        /// Finds the registered handler for the given query and invokes it. Throws when no handler is registered.
        /// </summary>
        /// <typeparam name="TQuery">The type of the query to handle.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="connection">The connection.</param>
        public async Task<QueryResult> HandleAsync<TQuery>(TQuery query, TConnection connection)
        {
            if (!_handlers.TryGetValue(query.GetType(), out var handler))
                throw new InvalidOperationException($"No handler is registered for query {typeof(TQuery).FullName}!");

            return await handler.Invoke(connection, query);
        }

    }
}
